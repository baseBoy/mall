import Vue from 'vue'
import Router from 'vue-router'
import login from '../components/login.vue'
import home from '../components/home.vue'
import welcome from '../components/welcome.vue'
import users from '../components/user/user.vue' // 用户权限
import roles from '../components/perm/roles.vue' // 角色列表
import rights from '../components/perm/rights.vue' // 权限列表
import goods from '../components/goods/goods.vue' // 商品列表
import params from '../components/goods/params.vue' // 分类参数
import categories from '../components/goods/categories.vue' // 商品分类
import orders from '../components/order/orders.vue' // 订单
import reports from '../components/report/reports.vue' // 报表

Vue.use(Router)

const router = new Router({
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', component: login },
    {
      path: '/home',
      component: home,
      redirect: '/welcome',
      children: [
        { path: '/welcome', component: welcome },
        { path: '/users', component: users },
        { path: '/roles', component: roles },
        { path: '/rights', component: rights },
        { path: '/goods', component: goods },
        { path: '/params', component: params },
        { path: '/categories', component: categories },
        { path: '/orders', component: orders },
        { path: '/reports', component: reports }
      ]
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  let tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})

export default router
